/*
** Define the last post image as main background
*/
function addBg() {
    var teste;
    teste = $( ".wrapper-posts" ).first().find( "#image-post" ).attr('src');
    $('#bg_container').css('background-image', 'url('+teste+')').before();
}

/*
** Push content down on menu hover
*/
function pushContentDown() {
    $('.nav').mouseover(function(){
        $('.wrapper-posts').css('margin-top', '275px');
    });

    $('.nav').mouseout(function(){
        $('.wrapper-posts').removeAttr('style');
    });
}

$( document ).ready(function() {
    addBg();
    pushContentDown();
});
